package vista;

import data.Grafo;
import data.Nodo;
import java.util.ArrayList;
import java.util.Scanner;

public class view {

    Scanner reader = new Scanner(System.in);

    public void imprimirGrafo(Grafo grafo) {
        System.out.println(grafo);
    }

    public int getNumeroVertices() {
        int v = reader.nextInt();
        return v;
    }

    public int getNumeroAristas() {
        int e = reader.nextInt();
        return e;
    }

    public int getFin() {
        int fin = reader.nextInt();
        return fin;
    }

    public int getOrigen() {
        int origen = reader.nextInt();
        return origen;
    }

    public void automorfismos() {
        System.out.println("Aut(grafo):");
    }

    public void pedirAristas() {
        System.out.println("Ingrese las aristas:");
    }

    public void pedirNumeroAristas() {
        System.out.println("Ingrese el numero de aristas:");
    }

    public void pedirNumeroVertices() {
        System.out.println("Ingrese el numero de vertices:");
    }
    
}
