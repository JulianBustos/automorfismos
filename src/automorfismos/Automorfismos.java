package automorfismos;

import data.Grafo;
import data.Nodo;
import gui.App;
import java.util.ArrayList;
import java.util.Scanner;
import vista.view;

public class Automorfismos {

    private static Scanner reader;
    private static String string;
    private static Grafo grafo;
    private static view view;

    public Automorfismos() {
        reader = new Scanner(System.in);
        string = new String("e");
        grafo = new Grafo();
        view = new view();
    }

    public Grafo getGrafo() {
        return grafo;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public static Grafo cambiar(Grafo grafo, int a, int b) {
        Nodo nodoa = grafo.getNodos().get(a - 1);
        nodoa.setVertice(b);
        return grafo;
    }

    public static Grafo llenar(Grafo grafo) {
        Grafo grafo2 = new Grafo();

        for (int i = 0; i < grafo.getNodos().size(); i++) {
            Nodo nodo = new Nodo(grafo.getNodos().get(i).getVertice());
            grafo2.addNodo(nodo);
        }
        for (int i = 0; i < grafo2.getNodos().size(); i++) {
            for (Nodo E : grafo.getNodos().get(i).getAristas()) {
                grafo2.getNodos().get(i).addArista(grafo2.getNodos().get(E.getVertice() - 1));
            }
        }
        return grafo2;
    }

    public static void imprimirPermutacion(ArrayList<Nodo> permutacion) {
        int[] paso = new int[permutacion.size()];
        for (int i = 1; i <= permutacion.size(); i++) {
            int vertice = i;
            if (permutacion.get(i - 1).getVertice() != vertice) {
                if (paso[i - 1] != -1) {
                    string = string + "(" + vertice;
                    vertice = permutacion.get(i - 1).getVertice();
                    while (vertice != i) {
                        string = string + vertice;
                        int pre = vertice;
                        vertice = permutacion.get(vertice - 1).getVertice();
                        paso[pre - 1] = -1;
                    }
                    string = string + ")";
                }
            }
        }
        string = string + "\n";
    }

    public static void generarPermutacion(Grafo grafo, ArrayList<Nodo> permutacion, int vertices, int contador) {
        ArrayList<Nodo> aux = new ArrayList<>();
        for (int i = 0; i < permutacion.size(); i++) {
            aux.add(permutacion.get(i));
        }
        if (contador == vertices) {
            Grafo grafo2 = new Grafo();
            grafo2 = llenar(grafo);
            for (int i = 0; i < permutacion.size(); i++) {
                grafo2 = cambiar(grafo2, permutacion.get(i).getVertice(), grafo.getNodos().get(i).getVertice());
            }
            if (grafo.equals(grafo2)) {
                imprimirPermutacion(permutacion);
            }
            return;
        } else {
            for (int i = 0; i < vertices; i++) {
                if (!permutacion.contains(grafo.getNodos().get(i))) {
                    permutacion = (ArrayList<Nodo>) aux.clone();
                    permutacion.add(grafo.getNodos().get(i));
                    generarPermutacion(grafo, permutacion, vertices, contador + 1);
                }
            }
        }
    }

    public void llenarVertices(int n) {
        for (int i = 0; i < n; i++) {
            Nodo nodo = new Nodo(i + 1);
            grafo.addNodo(nodo);
        }
    }

    public void adherirArista(int origen, int fin) {
        grafo.getNodos().get(origen - 1).addArista(grafo.getNodos().get(fin - 1));
        grafo.getNodos().get(fin - 1).addArista(grafo.getNodos().get(origen - 1));
    }

    public String calcularAutomorfismos() {
        ArrayList<Nodo> permutacion = new ArrayList<>();
        generarPermutacion(grafo, permutacion, grafo.getNodos().size(), 0);
        return string;
    }

    public static void main(String args[]) {
        if (args.length == 0) {
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
            /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
             */
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(App.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            //</editor-fold>

            /* Create and display the form */
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new App().setVisible(true);
                }
            });
        } else {
            Automorfismos A = new Automorfismos();

            // lectura
            view.pedirNumeroVertices();
            int v = view.getNumeroVertices();
            A.llenarVertices(v);
            view.pedirNumeroAristas();
            int e = view.getNumeroAristas();
            view.pedirAristas();
            for (int i = 0; i < e; i++) {
                A.adherirArista(view.getOrigen(), view.getFin());
            }
            view.imprimirGrafo(grafo);

            // transformaciones
            view.automorfismos();
            ArrayList<Nodo> permutacion = new ArrayList<>();
            generarPermutacion(grafo, permutacion, v, 0);
            System.out.println(string);

        }
    }

}
